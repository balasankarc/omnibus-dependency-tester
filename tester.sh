#!/bin/env bash

pushd /tmp/omnibus-gitlab || exit 0

bundle install

for software in config/software/*.rb; do
  name=$(basename "$software" | cut -d '.' -f1)
  echo "Doing ${name}"
  rm -rf config/projects/simple.rb /opt/simple /var/cache/omnibus/build /var/cache/omnibus/src
  cp /tmp/simple.rb config/projects/simple.rb
  sed -i "s/dependency.*/dependency '${name}'/i" config/projects/simple.rb
  bundle exec omnibus build simple || echo "${name}" >> failed.txt
done

if [ -f "failed.txt" ]; then
  echo "The following software definitions are incomplete:"
  cat failed.txt
  exit 1
else
  echo "All software definitions validated and found no errors"
  exit 0
fi
